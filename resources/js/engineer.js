$(document).ready(function(){

	//SE Tools list accordion

	var levelOneItems = $(".se-tools-section li.level-one");
	levelOneItems.each(function() {
		$(this).unbind().click(function(e) {
			e.preventDefault();

			if ($(this).hasClass("uncollapsed")) {
				$(this).removeClass("uncollapsed");
				$("ul", this).slideUp(200);
			} else {
				var uncollapsedItem = $(this).siblings(".uncollapsed");

				$(uncollapsedItem).removeClass("uncollapsed");
				$("ul", uncollapsedItem).slideUp(200);

				$(this).addClass("uncollapsed");
				$("ul", this).slideDown(200);
			}
		});
	});


	//Weapons hover effects
	//If the device supports touch (in other words, cannot hover with mouse), add tap events

	if($("html").hasClass("touch")){
		$(".weapon a").each(function() {
	    	$(this).unbind().click(function(e) {
				e.preventDefault();
	    		if ($(this).hasClass("tapped")) {
	    			console.log("removing class");
	    			var tappedItem = $("a.tapped", $(".weapon"));
					$(tappedItem).removeClass("tapped");
					$(".weapon a").removeClass("tapped");
					$(this).removeClass("tapped");
				} else {
					console.log("adding class");
					var tappedItem = $("a.tapped", $(".weapon"));
					$(tappedItem).removeClass("tapped");
					$(this).addClass("tapped");
				}
	    	});
		});
	} else {
		$(".weapon a").each(function() {
			$(this).addClass("has-hover");
	    	$(this).unbind().click(function(e) {
				e.preventDefault();
	    	});
		});
	}
	

	//Agile section horizontal animation
	$(window).scroll(function() {
		$(".scroll-animated").each(function(){
			var pos = $(this).offset().top;

			var winTop = $(window).scrollTop();
			if (pos < winTop + 900) {
				$(this).addClass("slide");
			}
		});
	});
});