$(document).ready(function() {
    //In order to force the menu to collapse if the window size changes to non-smartphone.
    $(window).resize(checkSize);

    //Collapsible menu
    $("li.collapse-icon").unbind().click(function(e) {
        e.preventDefault();
        $(".main-menu-collapsible-overlay").slideToggle(300);
    });

    $(".main-menu-collapsible-overlay").unbind().click(function(e) {
         $(".main-menu-collapsible-overlay").slideUp(300);
    });

    $(".main-menu-collapsible li a").unbind().click(function(e) {
        $(".main-menu-collapsible-overlay").slideUp(300);
    });


    //Smooth scroll to a certain anchor
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: target.offset().top
                }, 500);
                return false;
            }
        }
    });
});

//Checking just the window size has still bugs in older browsers, so this hack seems better.
function checkSize(){
    //If the normal menu's position is flex, then the size is that of a smartphone
    var sizeIsSmartphone = ($(".main-menu").css("display") == "flex");

    if (!sizeIsSmartphone){
        $(".main-menu-collapsible-overlay").hide();
    }
}