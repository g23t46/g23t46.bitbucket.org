$(document).ready(function () {
	//Swiper  
	var mySwiper = new Swiper ('.swiper-container', {
		pagination: '.swiper-pagination',
		keyboardControl: true,
        slidesPerView: 'auto',
        centeredSlides: true,
        paginationClickable: true,
        noSwiping: false,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        grabCursor: true,
        spaceBetween:140,
        breakpoints: {
                640: {
                        spaceBetween:50
                },
                768: {
                        spaceBetween:80
                },
                1024: {
                        spaceBetween:100
                },
                1440: {
                        spaceBetween:120
                }
        }
    });



    //Shuffle  

    var Shuffle = window.shuffle;
    var Viewport = window.Viewport;

    var element = $('.photopholio-grid')[0];

    window.myShuffle = new Shuffle(element, {
        itemSelector: '.photopholio-item',
        sizer: '.column-sizer',
    });



    //SwipeBox

    var photoSwipeGallery;

    var initPhotoSwipeFromDOM = function(gallerySelector) {

        // parse slide data (url, title, size ...) from DOM elements 
        // (children of gallerySelector)
        var parseThumbnailElements = function(gallery) {

            var thumbnails = $(".photopholio-item", gallery);
            var thumbnailCount = thumbnails.length;
            var imageInfoCollection = [];
            var thumbnailLink, thumbnailSize, imageInfo;


            for(var i = 0; i < thumbnailCount; i++) {

                thumbnail = thumbnails[i];

                // include only element nodes 
                if(thumbnail.nodeType !== 1) {
                    continue;
                }

                thumbnailLink = $("a", thumbnail); // <a> element

                thumbnailSize = thumbnailLink.attr("data-size").split('x');

                // create image info item
                imageInfo = {
                    src: thumbnailLink.attr("href"),
                    w: parseInt(thumbnailSize[0], 10),
                    h: parseInt(thumbnailSize[1], 10)
                };


                // <img> thumbnail element, retrieving thumbnail url
                imageInfo.msrc = $("img", thumbnailLink).attr("src");

                imageInfo.thumbnail = thumbnail; // save link to element for getThumbBoundsFn
                imageInfoCollection.push(imageInfo);
            }

            return imageInfoCollection;
        };

        // find nearest parent element
        var closest = function closest(element, elementIsFigure) {
            return element && ( elementIsFigure(element) ? element : closest(element.parentNode, elementIsFigure) );
        };

        // triggers when user clicks on thumbnail
        var onThumbnailsClick = function(e) {
            e = e || window.event;
            e.preventDefault ? e.preventDefault() : e.returnValue = false;

            var eTarget = e.target || e.srcElement;

            // find thumbnail of clicked element
            var clickedThumbnail = closest(eTarget, function(element) {
                return (element.tagName && element.tagName.toUpperCase() === 'FIGURE');
            });

            if(!clickedThumbnail) {
                return;
            }

            // find index of clicked item by looping through all child nodes
            // alternatively, you may define index via data- attribute
            var gallery = clickedThumbnail.parentNode;
            var thumbnails = $(".photopholio-item", gallery);
            var thumbnailCount = thumbnails.length;
            var thumbnailIndex = 0;
            var clickedThumbnailIndex;

            for (var i = 0; i < thumbnailCount; i++) {
                if(thumbnails[i].nodeType !== 1) { 
                    continue; 
                }

                if(thumbnails[i] === clickedThumbnail) {
                    clickedThumbnailIndex = thumbnailIndex;
                    break;
                }
                thumbnailIndex++;
            }



            if(clickedThumbnailIndex >= 0) {
                // open PhotoSwipe if valid index found
                openPhotoSwipe(clickedThumbnailIndex, gallery);
            }
            return false;
        };

        // parse picture index and gallery index from URL (#&pid=1&gid=2)
        var photoswipeParseHash = function() {
            var hash = window.location.hash.substring(1),
            params = {};

            if(hash.length < 5) {
                return params;
            }

            var vars = hash.split('&');
            for (var i = 0; i < vars.length; i++) {
                if(!vars[i]) {
                    continue;
                }
                var pair = vars[i].split('=');  
                if(pair.length < 2) {
                    continue;
                }           
                params[pair[0]] = pair[1];
            }

            if(params.gid) {
                params.gid = parseInt(params.gid, 10);
            }

            return params;
        };

        var openPhotoSwipe = function(index, gallery, disableAnimation, fromURL) {
            var pswpElement = $(".pswp")[0];
            var thumbnails = parseThumbnailElements(gallery);

            // define options (if needed)
            var options = {

                // define gallery index (for URL)
                galleryUID: gallery.getAttribute('data-pswp-uid'),

                getThumbBoundsFn: function(index) {
                    // See Options -> getThumbBoundsFn section of documentation for more info
                    var thumbnail = $("img", thumbnails[index].thumbnail)[0]; // find thumbnail
                    var pageYScroll = window.pageYOffset || document.documentElement.scrollTop;
                    console.log(thumbnail);
                    var rect = thumbnail.getBoundingClientRect(); 

                    return {x:rect.left, y:rect.top + pageYScroll, w:rect.width};
                },

                arrowKeys: true,
                history: false
            };

            // PhotoSwipe opened from URL
            if(fromURL) {
                if(options.galleryPIDs) {
                    // parse real index when custom PIDs are used 
                    // http://photoswipe.com/documentation/faq.html#custom-pid-in-url
                    for(var j = 0; j < thumbnails.length; j++) {
                        if(thumbnails[j].pid == index) {
                            options.index = j;
                            break;
                        }
                    }
                } else {
                    // in URL indexes start from 1
                    options.index = parseInt(index, 10) - 1;
                }
            } else {
                options.index = parseInt(index, 10);
            }

            // exit if index not found
            if( isNaN(options.index) ) {
                return;
            }

            if(disableAnimation) {
                options.showAnimationDuration = 0;
            }

            // Pass data to PhotoSwipe and initialize it
            photoSwipeGallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, thumbnails, options);
            photoSwipeGallery.init();
            photoSwipeGallery.listen('imageLoadComplete', function(index, item) { 
                $(".main-navbar").css("visibility", "hidden");
            });
            photoSwipeGallery.listen('close', function() { 
                $(".main-navbar").css("visibility", "visible");
            });
        };

        // loop through all gallery elements and bind events
        var galleries = $( gallerySelector );

        for(var i = 0, l = galleries.length; i < l; i++) {
            galleries[i].setAttribute('data-pswp-uid', i+1);
            galleries[i].onclick = onThumbnailsClick;
        }

        // Parse URL and open gallery if it contains #&pid=3&gid=1
        var hashData = photoswipeParseHash();
        if(hashData.pid && hashData.gid) {
            openPhotoSwipe( hashData.pid ,  galleryElements[ hashData.gid - 1 ], true, true );
        }
    };

    // execute above function
    initPhotoSwipeFromDOM('.photopholio-grid');

    // listen to events

    
});